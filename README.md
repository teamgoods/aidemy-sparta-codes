# Aidemy Sparta塾　codes #

### ReadMe
* awscliまたはguiでs3上からデータセットを取得する
* tarファイル(im2txt/IAPR/iapr_images.tar.gzおよびim2txt/COCO/coco_images.tar.gz)を解凍し、imagesディレクトリが生成される
`$tar -zxvf xxx.tar.gz`
* gitなどからコードをダウンロードする
* ファイルの配置を下記のようにする

![構成図](https://s3-ap-northeast-1.amazonaws.com/readme-img/img/sparta.png)

* 注意点：またarticlesに関して、azure上でarticlesの読み出しにバグが起きたが、原因は結局不明だった。環境によってはarticlesを使うコード(ch4/word2vec, ch4/article_classification)が動かない可能性がある。


### ch1 ###

* 01_DNN_MNIST_Tutorial.ipynb
* 02_DNN_MNIST_Work.ipynb
* 03_DNN_CIFAR10_Work1.ipynb
* 04_DNN_CIFAR10_Work2.ipynb

* CNN_CIFAR_hyperas

### ch2 ###

* 01_CNN_MNIST_Tutorial.ipynb
* 02_CNN_CIFAR10_Work1.ipynb
* 03_CNN_CIFAR10_Work2.ipynb
* 04_CNN_CIFAR100_Work.ipynb
* 05_CNN_GoogleOpenImage_Work.ipynb

### ch3 ###

* 01_RNN_sineWave_Tutorial.ipynb
* 02_
* 03_RNN_UKgas_Work.ipynb
* 04_RNN_BTC_Work.ipynb
* 05_RNN_MNIST_Work?

### ch4 ###

* 01_Word2Vec_Tutorial.ipynb
* 02_NLP_SPAM_Tutorial.ipynb
* 03_NLP_Article_Classification_Work.ipynb
* 04_NLP_Text_Generation_Work.ipynb
* 05_Seq2seq_Dialogue_Work.ipynb?
* 06_Seq2seq_Text_generation.ipynb?

### ch5 ###

* 01_FunctionalAPI_Tutorial.ipynb
* 02_im2txt.ipynb?
