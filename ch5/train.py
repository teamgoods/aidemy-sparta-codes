from evaluator import Evaluator
from generator import Generator
from keras.models import load_model
from keras.callbacks import CSVLogger
from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from models import NIC
from data_manager import DataManager

import sys


num_epochs = 100
batch_size = 512

root_path = '../datasets/IAPR_2012/'
#root_path = '../datasets/COCO/'

captions_filename = root_path + 'IAPR_2012_captions.txt'
#captions_filename = root_path + 'COCO_training_2014_captions.txt'

data_manager = DataManager(data_filename=captions_filename,
                            max_caption_length=30,
                            word_frequency_threshold=2,
                            extract_image_features=True,
                            #cnn_extractor='inception',
                            cnn_extractor='vgg16',
                            image_directory=root_path,
                            #image_directory=root_path,
                            split_data=True,
                            dump_path=root_path + 'preprocessed_data/')

data_manager.preprocess()
print(data_manager.captions[0])
print(data_manager.word_frequencies[0:20])

preprocessed_data_path = root_path + 'preprocessed_data/'
generator = Generator(data_path=preprocessed_data_path,
                      batch_size=batch_size,
                      image_features_filename='../datasets/IAPR_2012/preprocessed_data/vgg16_image_name_to_features.h5')
                      #image_features_filename=root_path + 'preprocessed_data/inception_image_name_to_features.h5')

num_training_samples =  generator.training_dataset.shape[0]
num_validation_samples = generator.validation_dataset.shape[0]
print('Number of training samples:', num_training_samples)
print('Number of validation samples:', num_validation_samples)

if len(sys.argv) == 1:
    model = NIC(max_token_length=generator.MAX_TOKEN_LENGTH,
                vocabulary_size=generator.VOCABULARY_SIZE,
                rnn='lstm',
                num_image_features=generator.IMG_FEATS,
                hidden_size=1024,
                embedding_size=1024)
else:
    model_filename = sys.argv[1]
    model = load_model(model_filename)


model.compile(loss='categorical_crossentropy',
              optimizer = 'adam',
              metrics=['accuracy'])

print(model.summary())
print('Number of parameters:', model.count_params())

training_history_filename = preprocessed_data_path + 'training_history.log'
csv_logger = CSVLogger(training_history_filename, append=False)

model_names = ('../trained_models/IAPR_2012/' + 'iapr_weights.{epoch:02d}-{val_loss:.2f}.hdf5')
#model_names = ('../trained_models/COCO/' + 'coco_weights.{epoch:02d}-{val_loss:.2f}.hdf5')

model_checkpoint = ModelCheckpoint(model_names,
                                   monitor='val_loss',
                                   verbose=1,
                                   save_best_only=False,
                                   save_weights_only=False)

reduce_learning_rate = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                         patience=5, verbose=1)

callbacks = [csv_logger, model_checkpoint, reduce_learning_rate]

model.fit_generator(generator=generator.flow(mode='train'),
                    steps_per_epoch=int(num_training_samples / batch_size),
                    #steps_per_epoch=5,
                    epochs=num_epochs,
                    verbose=1,
                    callbacks=callbacks,
                    validation_data=generator.flow(mode='validation'),
                    validation_steps=int(num_validation_samples / batch_size))
                    #validation_steps=3)

#evaluator = Evaluator(model, data_path=preprocessed_data_path,
#                      images_path=root_path + 'iaprtc12/')

#evaluator.display_caption()
#evaluator.display
